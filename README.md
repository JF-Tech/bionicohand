# Bionicohand

Bionico hand is an open-source, 3D-printed myoelectric prosthesis. This project aims to collaboratively create a "generic" hand prosthesis that is accepted by all users (weight, control, appearance) and that is repairable yourself. This prosthesis must be affordable in price, in other words a happy medium between the very expensive elaborate prostheses and the prostheses made are even currently too brittle.

<p align="center">
  <img src="https://i.goopics.net/b4t64r.png">
</p>


## Open Source

All the technological development work made in the Bionicohand project is freely available under open-source licenses.
Please keep references to the Bionicohand project (https://myhumankit.org/#) and authors when you use or fork this work.


## Project file and their content

- [ ] [Archive](https://gitlab.com/bionico/bionicohand/-/tree/main/Archive) : All previous works, some of which have been retained for the current solution and others not

- [ ] [CAO](https://gitlab.com/bionico/bionicohand/-/tree/main/CAO) : Modeling of the myoelectric hand
	
- [ ] [Technical documentation Gitlab and Sourcetree](https://gitlab.com/bionico/bionicohand/-/tree/main/documentation%20technique%20gitlab%20source%20tree) : 2 documentations explaining the operation of the software and the accesses according to the statuses

- [ ] [Electronics](https://gitlab.com/bionico/bionicohand/-/tree/main/Electronique) : Integration of the electronic part of the myoelectric hand

- [ ] [Manufacturing](https://gitlab.com/bionico/bionicohand/-/tree/main/Manufacturing) : Drawing and inventory of the necessary parts

- [ ] [Instruction](https://gitlab.com/bionico/bionicohand/-/tree/main/Instruction) : Explanation of how certain parts of the hand work

- [ ] [Arborescence_GitLab_Bionicohand](https://gitlab.com/bionico/bionicohand/-/blob/main/Arborescence_GitLab_Bionicohand.pdf) : Data classification overview PDF File


## Support

Do you need assistance? On the site of My Human Kit, carrier of the project, a tab to ask for help is present (https://myhumankit.org/contact/).


## Contribution

If you are interested in contributing to the Bionicohand project, you can use the files to contribute. Then, you can put your progress on the gitlab as explained in the technical documentation mentioned above.

In addition, the Bionicohand project involves a very wide range of disciplines:

- Engineering fields such as IT, mechanics, electronics, CAD...
- Social sciences such as cognitive sciences, psychology...
- Life sciences such as biology, biomechanics,...
- Community management, scientific mediation, communication...
- Design such as web design, object design...

So there are many ways to contribute to this project and you are welcome.

## More on the project

<center>

| [![Logo MHK](https://i.goopics.net/mateon.png)](https://myhumankit.org/) | [![Logo facebook](https://www.hebergeur-image.com/upload/195.101.70.141-64ac14bea129f.png)](https://www.facebook.com/bionicohand/?locale=fr_FR) | [![Logo twitter](https://www.hebergeur-image.com/upload/195.101.70.141-64ac15a72fe4c.png)](https://twitter.com/Bionicohand)  | [![Logo youtube](https://www.hebergeur-image.com/upload/195.101.70.141-64ac167b721c5.png)](https://www.youtube.com/channel/UCHg821Sk8pQTGZ-V9kBZ9KA) |
| ------------- | :-------------: | :-------------: | :-------------: |

</center>


